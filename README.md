# Tablas RESTful

Este proyecto consiste en una API RESTFUL implementada en Node.js utilizando Express.js en la que se puede realizar diversas operaciones matemáticas utilizando una tabla RESTful.

## Autor
Jair Delval Aguirre. 355274

Tabla RESTFull:
- GET /results/:n1/:n2 -> Sumar n1 + n2
- POST /results/ -> Multiplicar n1 * n2
- PUT /results/ -> Dividir n1 / n2
- PATCH /results/ -> Potencia n1 ^ n2
- DELETE /results/:n1/:n2 -> restar n1 - n2
