const express = require('express');
const app = express();
const PORT = 3000; 

app.get('/results/:n1/:n2', (req, res) => {
  const { n1, n2 } = req.params;
  const sum = parseFloat(n1) + parseFloat(n2);
  res.send(`La suma de ${n1} y ${n2} es ${sum}`);
});

app.post('/results', (req, res) => {
  const { n1, n2 } = req.query;
  const mult = parseFloat(n1) * parseFloat(n2);
  res.send(`El resultado de multiplicar ${n1} por ${n2} es ${mult}`);
});

app.put('/results', (req, res) => {
  const { n1, n2 } = req.query;
  const div = parseFloat(n1) / parseFloat(n2);
  res.send(`El resultado de dividir ${n1} entre ${n2} es ${div}`);
});

app.patch('/results', (req, res) => {
  const { n1, n2 } = req.query;
  const pot = Math.pow(parseFloat(n1), parseFloat(n2));
  res.send(`El resultado de elevar ${n1} a la potencia ${n2} es ${pot}`);
});

app.delete('/results/:n1/:n2', (req, res) => {
  const { n1, n2 } = req.params;
  const men = parseFloat(n1) - parseFloat(n2);
  res.send(`La resta de ${n1} menos ${n2} es ${men}`);
});

app.listen(PORT, () => {});
